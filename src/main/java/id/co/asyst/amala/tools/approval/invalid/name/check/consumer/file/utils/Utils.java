package id.co.asyst.amala.tools.approval.invalid.name.check.consumer.file.utils;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    private static Logger logger = LogManager.getLogger(Utils.class);

    public void validationActivemq(Exchange exchange) throws ParseException {
        logger.info("validation....");

        int mq= 0;
        logger.info("identity :" + exchange.getProperty("identitymq"));

        if (exchange.getProperty("identitymq") != null && !exchange.getProperty("identitymq").equals("")) {
            logger.info("validation identity activemq");
            mq =1;
        } else {
            mq = 0;
        }

        exchange.setProperty("reqmqvalid", mq);
        logger.info("reqmqvalid :" + exchange.getProperty("reqmqvalid"));
    }

    public void datetime(Exchange exchange) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("date", sdf2.format(createddate));
        logger.info("method datetime");
    }

    public void data(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        List<Map<String, Object>> body = in.getBody(List.class);
        logger.info("body 2nd query : " + body);
        String activityinfo = (String) body.get(0).get("activityinfo");
        String activityid = (String) body.get(0).get("activityid");
        String cardnumber = (String) body.get(0).get("cardnumber");
        String memberid = (String) body.get(0).get("memberid");
        String bookingpersonalias = (String) body.get(0).get("bookingpersonalias");

        exchange.setProperty("activityinfo", activityinfo);
        exchange.setProperty("activityid", activityid);
        exchange.setProperty("cardnumber", cardnumber);
        exchange.setProperty("memberid", memberid);
        exchange.setProperty("bookingpersonalias", bookingpersonalias);

    }

    public void activityidmq(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
//        List<Map<String, Object>> body = in.getBody(List.class);
        List<String> activityidmq = (List<String>) exchange.getProperty("activityidmq");
//        List<Map<String, Object>> body = (List<Map<String, Object>>) exchange.getProperty("activityidmq");
        logger.info("activityid after : " + activityidmq);
        String activityid = (String) activityidmq.get(0);
        exchange.setProperty("activityidafter", activityid);
        logger.info("activityid after method : " + activityid);

    }

    public void getActivityid(Exchange exchange) {
        logger.info("getActivityid ");
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Map<String, Object> body = in.getBody(Map.class);
        String activityid = (String) body.get("activityid");
        exchange.setProperty("activityidonsplit", activityid);
        logger.info("activityidonsplit : " + exchange.getProperty("activityid"));

    }

    public void setIdentity (Exchange exchange) {

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("createddate", sdf2.format(createddate));
        UUID uuid = UUID.randomUUID();

        LinkedHashMap<String, Object> identity = new LinkedHashMap<String, Object>();
        String apptxnid = "";
        String reqtxnid = uuid.toString();
        String signature = "";

        identity.put( "apptxnid", apptxnid);
        identity.put( "reqtxnid", reqtxnid);
        identity.put( "reqdate", exchange.getProperty("createddate"));
        identity.put( "appid", exchange.getProperty("appid"));
        identity.put( "userid", exchange.getProperty("userid"));
        identity.put( "password", exchange.getProperty("password"));
        identity.put( "signature", signature);
        identity.put( "seqno", exchange.getProperty("seqno"));

        logger.info( "Identity ::: " +identity);
        exchange.setProperty("identity", identity);


    }

    public static String GenerateId(String prefix, Date date, int numdigit) {


        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");

        Random rand = new Random();

        String dateFormat = "";

        if (date != null)
            dateFormat = sdf.format(date);

        String[] arr = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i",
                "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

        String randomkey = "";
        for (int i = 0; i < numdigit; i++) {

            int random = rand.nextInt(36);
            randomkey += arr[random];
        }

        if (prefix== null){
            prefix="APPRV";
        }

        return prefix + dateFormat + randomkey;
    }

    public void setId (Exchange exchange){
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String id;
        Date d = new Date();
        id = GenerateId("APPRV", d, 12);
        exchange.setProperty("id", id);



    }

    public void setHeader (Exchange exchange) {
        Message out = exchange.getOut();
        Map<String, Object> header = new LinkedHashMap<>();
        header.put("activityid", "activityid");
        header.put("activityinfo", "activityinfo");
        header.put("activitytype", "activitytype");
        header.put("activitydate", "activitydate");
        header.put("cardnumber", "cardnumber");
        header.put("memberid", "memberid");
        header.put("bookingpersonalias", "bookingpersonalias");
        header.put("member_name1", "member_name1");
        header.put("member_name2", "member_name2");
        header.put("status", "status");
        header.put("marketingairlinecode", "marketingairlinecode");
        header.put("marketingfltnum", "marketingfltnum");
        header.put("origin", "origin");
        header.put("destination", "destination");
        header.put("statusprocess", "process");
        header.put("remark", "remark");

        out.setBody(header);
    }

    public void setName (Exchange exchange) {
        String filename = exchange.getProperty("filename").toString();
        String finalname = filename.replace(".csv", "");
        logger.info("cek finalname :::" +finalname);
        exchange.setProperty("filename", finalname);
    }

}
