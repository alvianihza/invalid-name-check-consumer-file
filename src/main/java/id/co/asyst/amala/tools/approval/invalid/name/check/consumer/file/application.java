package id.co.asyst.amala.tools.approval.invalid.name.check.consumer.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:main.xml")
@ComponentScan({"id.co.asyst.amala.tools.approval.invalid.name.check.consumer"})


public class application {

    public static void main(String[] args) {
        SpringApplication.run(application.class, args);
    }
}
