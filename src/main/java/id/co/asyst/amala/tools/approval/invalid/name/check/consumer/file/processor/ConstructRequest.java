package id.co.asyst.amala.tools.approval.invalid.name.check.consumer.file.processor;

import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

public class ConstructRequest {

    private static Logger logger = LogManager.getLogger(ConstructRequest.class);

    public void reqDataFile (Exchange exchange){
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        String activityidin = in.getBody().toString();
        logger.info("activityid on split :::" +activityidin);
        Gson gson = new Gson();

        LinkedHashMap<String, Object> getServiceApprovedName = new LinkedHashMap<String, Object>();
        Map<String, Object> parameter = new HashMap<String, Object>();
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        Map<String, Object> data = new HashMap<String, Object>();
        List<String> activityid = new ArrayList<String>();

        activityid.add(activityidin);

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        Date createddate;
        createddate = d;
        exchange.setProperty("dateend", sdf2.format(createddate));

        out.setHeader("id", exchange.getProperty("id"));
        out.setHeader("filename", exchange.getProperty("filename"));

        data.put("activityid", activityid);
        data.put("actiontype", "APPROVED");
        parameter.put("data", data);
        getServiceApprovedName.put("identity", identity);
        getServiceApprovedName.put("parameter", parameter);
        String result = gson.toJson(getServiceApprovedName);
        out.setBody(result);
        logger.info(result);
    }
}
