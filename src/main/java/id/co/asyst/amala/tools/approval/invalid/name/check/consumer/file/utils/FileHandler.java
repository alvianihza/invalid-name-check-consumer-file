package id.co.asyst.amala.tools.approval.invalid.name.check.consumer.file.utils;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FileHandler {

    private static Logger logger = LogManager.getLogger(FileHandler.class);

    public void readFile (Exchange exchange){
        logger.info("readFile method");
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        logger.info("Body :::" +in);
        List<String> activityid = new ArrayList<String>();
        List<List<String>> data = (List<List<String>>) exchange.getIn().getBody();
        for (List<String> line : data) {
            activityid.add(String.format(line.get(0)));
//            logger.info(activityid);
//            logger.info(String.format("%s has an IQ of %s and is currently %s", line.get(0), line.get(1), line.get(2)));
        }
        out.setBody(activityid);
    }

    public void outFile (Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        List<Map<String, Object>> body = in.getBody(List.class);
        logger.info("method outFile body ::: " +body);
        Map<String, Object> header = new LinkedHashMap<>();
//        List<Map<String, Object>> fileout = new ArrayList<Map<String, Object>>();
//        fileout.add(body.get(0));
        Map<String, Object> fileout = new LinkedHashMap<>();
        fileout.put("activityid", body.get(0).get("activityid"));
        fileout.put("activityinfo", body.get(0).get("activityinfo"));
        fileout.put("activitytype", body.get(0).get("activitytype"));
        fileout.put("activitydate", body.get(0).get("activitydate"));
        fileout.put("cardnumber", body.get(0).get("cardnumber"));
        fileout.put("memberid", body.get(0).get("memberid"));
        fileout.put("bookingpersonalias", body.get(0).get("bookingpersonalias"));
        fileout.put("member_name1", body.get(0).get("member_name1"));
        fileout.put("member_name2", body.get(0).get("member_name2"));
        fileout.put("status", body.get(0).get("status"));
        fileout.put("marketingairlinecode", body.get(0).get("marketingairlinecode"));
        fileout.put("marketingfltnum", body.get(0).get("marketingfltnum"));
        fileout.put("origin", body.get(0).get("origin"));
        fileout.put("destination", body.get(0).get("destination"));
        fileout.put("statusprocess", exchange.getProperty("process"));
        fileout.put("remark", exchange.getProperty("responsemessage"));

//        fileout.add("activityid", body.get(0).get("activityid").toString());
//        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-mm-dd");
//
//        LocalDate currentDate = LocalDate.now();
//        LocalDate expirationDate = LocalDate.now();
//
//
//        for(int i = 0; i < body.size(); i++) {
//            expirationDate = LocalDate.parse(body.get(i).get("expireddate").toString());
//
//            if (expirationDate.isBefore(currentDate)) {
//                expireddate.add(body.get(i));
//                expireddate.get(i).put("status "," already expired ");
//                expireddate.get(i).remove("trxid");
//                expireddate.get(i).remove("memberaccountid");
//                expireddate.get(i).remove("isexpired");
//                expireddate.get(i).remove("extendable");
//                expireddate.get(i).remove("frequency");
//                expireddate.get(i).remove("tiermiles");
//
//
//            } else {
//                body.get(i).put(" status ", " Belum Expired ");
//            }
//        }

        out.setBody(fileout);
        logger.info("Data FileOut :::" +fileout);
    }

    public void invalidData (Exchange exchange) {
        Message out = exchange.getOut();
        Map<String, Object> fileout = new LinkedHashMap<>();
        fileout.put("activityid", exchange.getProperty("activityidafter"));
        fileout.put("activityinfo", "NO DATA");
        fileout.put("activitytype", "NO DATA");
        fileout.put("activitydate", "NO DATA");
        fileout.put("cardnumber", "NO DATA");
        fileout.put("memberid", "NO DATA");
        fileout.put("bookingpersonalias", "NO DATA");
        fileout.put("member_name1", "NO DATA");
        fileout.put("member_name2", "NO DATA");
        fileout.put("status", "NO DATA");
        fileout.put("marketingairlinecode", "NO DATA");
        fileout.put("marketingfltnum", "NO DATA");
        fileout.put("origin", "NO DATA");
        fileout.put("destination", "NO DATA");
        fileout.put("statusprocess", exchange.getProperty("process"));
        fileout.put("remark", exchange.getProperty("responsemessage"));
        out.setBody(fileout);
        logger.info("No Data FileOut :::" +fileout);
    }

}
